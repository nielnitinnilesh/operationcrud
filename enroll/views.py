from django.shortcuts import render, HttpResponseRedirect

from enroll.forms import Studentreg
from .models import User

# Create your views here.
def add_show(request):
    if request.method == 'POST':
        fm = Studentreg(request.POST)
        if fm.is_valid():
            nm = fm.cleaned_data['name']
            em = fm.cleaned_data['email']
            pw = fm.cleaned_data['password']
            reg = User(name=nm, email=em, password=pw)
            reg.save()
            fm = Studentreg()
    else:
        print('GET request!!!')
        fm = Studentreg()
    stud = User.objects.all()
    return render(request, 'enroll/addandshow.html', {'fm':fm, 'stu':stud})

# this function will update/edit
def update_data(request, id):
    if request.method == 'POST':
        pi = User.objects.get(pk=id)
        fm = Studentreg(request.POST, instance=pi)
        if fm.is_valid:
            fm.save()
    else:
        pi = User.objects.get(pk=id)
        fm = Studentreg(instance=pi)
    return render(request, 'enroll/updatestudent.html', {'form':fm})


# this function will delete data..
def delete_data(request, id):
    if request.method == 'POST':
        pi = User.objects.get(pk=id)
        pi.delete()
        return HttpResponseRedirect('/')